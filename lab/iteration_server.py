# -*- encoding: utf-8 -*-

import socket

# Ustawienie licznika na zero
counter = 0
# Tworzenie gniazda TCP/IP
socket = socket.socket()
# Powiązanie gniazda z adresem

server_address = ('194.29.175.240', 31018)  # TODO: zmienić port!
socket.bind(server_address)
# Nasłuchiwanie przychodzących połączeń
socket.listen(1)
while True:
    # Czekanie na połączenie
    connection, client = socket.accept()
    # Podbicie licznika
    counter += 1
    try:
        # Wysłanie wartości licznika do klienta
        connection.sendall(str(counter))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
