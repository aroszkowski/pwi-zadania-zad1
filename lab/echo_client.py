# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP P18, adres :194.29.175.240
socket = socket.socket()
# Połączenie z gniazdem nasłuchującego serwera
server_address = ('194.29.175.240', 31018)  # TODO: zmienić port!
socket.connect(server_address)
try:
    # Wysłanie danych
    message = u'To jest wiadomość, która zostanie zwrócona.'
    socket.sendall(message.encode('utf-8'))
    # Wypisanie odpowiedzi

    resp = socket.recv(4096)
    print str(resp)
finally:
    socket.close()
    pass